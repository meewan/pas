"use strict";

const DATA_KEY = 'pas-data-64a755500efe9b8275422e16f551bc06'

const FIELD_NAMES = [
    'firstname',
    'lastname',
    'birthday',
    'placeofbirth',
    'firstname',
    'birthday',
    'placeofbirth',
    'address',
    'city',
    'zipcode',
    'datesortie',
    'heuresortie'
];

function get_fields()
{

    const fields = {};
    for (let i = 0; i < FIELD_NAMES.length; i++)
    {
        let field_id = 'field-' + FIELD_NAMES[i];
        fields[FIELD_NAMES[i]] = document.getElementById(field_id);
    }
    return fields;
}

function get_checkbox()
{
    const checkbox_names = [
        'travail',
        'achats',
        'sante',
        'famille',
        'handicap',
        'sport_animaux',
        'convocation',
        'missions',
        'enfants'
    ];
    const checkbox = {};
    for (let i = 0; i < checkbox_names.length; i++)
    {
        let checkbox_id = 'checkbox-' + checkbox_names[i];
        checkbox[checkbox_names[i]] = document.getElementById(checkbox_id);
    }
    return checkbox;
}

function load_data() {
    const stored = window.localStorage.getItem(DATA_KEY);
    if(stored)
    {
        return JSON.parse(stored);
    }
    else
    {
        const data = {
            'fields': {},
            'checkbox': []
        };
        for (let field in FIELD_NAMES)
        {
            data['fields'][FIELD_NAMES[field]] = '';
        }
        return data;
    }


}

function save_data(fields, checkbox)
{
    console.debug('saving_data')
    const data = {
        'fields': {},
        'checkbox': []
    };
    for (let field in fields)
    {
        let value = fields[field];
        data['fields'][field] = value.value;
    }
    for (let box_name in checkbox)
    {
        let checked = checkbox[box_name].checked;
        if (checked)
        {
            data['checkbox'].push(box_name);
        }
    }
    window.localStorage.setItem(DATA_KEY, JSON.stringify(data));
    console.debug('data-saved')
}

function fill_form(fields, checkbox)
{
    const data = load_data();
    const date = new Date();
    for (let field in fields)
    {
        let element = fields[field];
        if (field === 'datesortie')
        {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            const date_string = year.toString() + '-' + month.toString() + '-' + day.toString();
            element.value = date_string;

        }
        else if (field === 'heuresortie')
        {
            const hours = date.getHours();
            const minutes = date.getMinutes();
            const time_string = hours.toString() + ':' + minutes.toString();
            element.value = time_string;
        }
        else
        {
            element.value = data['fields'][field];
        }
    }

    for (let box in checkbox)
    {
        if(data['checkbox'].includes(box))
        {
            checkbox[box].checked = true;
        }
        else
        {
            checkbox[box].checked = false;
        }
    }
}

function main(main)
{
    console.log('starting PAS');
    console.debug('retrieve fields');
    const fields = get_fields();
    console.debug('retrieve checkbox');
    const checkbox = get_checkbox();
    console.debug('retrieve generate_button');
    const generateButton = document.getElementById('generate-btn');
    console.debug('attach to validation');
    generateButton.onclick = function(event){save_data(fields, checkbox);}
    console.debug('trying to fill the form');
    fill_form(fields, checkbox);
    console.debug('setup done');
}

try
{
    main();
}
catch(error)
{
    console.error(error);
}
